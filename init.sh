

apt update && apt install -y redis-server lsb-release

export CODENAME=`lsb_release -cs`
echo "deb https://download.rethinkdb.com/repository/debian-$CODENAME $CODENAME main" |  tee /etc/apt/sources.list.d/rethinkdb.list
wget -qO- https://download.rethinkdb.com/repository/raw/pubkey.gpg |  apt-key add -
apt-get update
apt-get -y install rethinkdb ffmpeg imagemagick



git clone https://github.com/DankMemer/imgen
cd imgen/


# now copy the config.json
chmod +x start.sh
cp -rf example-config.json config.json

# open config.json and fill in the necessary credentials and save it.
# Now to install the requirements

python -m pip install --upgrade pip
pip install gunicorn
pip install -r requirements.txt

cd ..


FROM python:3.9.9-buster

WORKDIR /env

COPY . /env

RUN /env/init.sh

RUN sed -i s/127.0.0.1/0.0.0.0/g /env/imgen/start.sh

EXPOSE 65535

ENTRYPOINT ["/env/start.sh"]

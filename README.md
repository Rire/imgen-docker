# imgen-docker

https://github.com/DankMemer/imgen dockerized

## Usage

* $ cd /path/to/imgen-docker 
* $ chmod +x *.sh
* Edit config.json to supply token/ids
* \# docker build . -t imgen
* \# docker run -d -v /path/to/config.json/here -v/path/to/rethinkdb/here:/env/imgen/rethinkdb_data --name=imgen -p 65535:65535 imgen
* \# docker exec -it imgen python3 /env/setuprethink.py

* Enable callback OAuth2 URL in Discord developer panel under your bot https://yourdomain.com/callback
* Login to admin panel on your API site and gen API key for use with your bot(s).
